# Project Title

Control two steppers by arduino and python

## Description

Control two steppers by arduino Uno, CNC shield, A4988 (can be replace to stepper controller as TB6600).
Control stepper by sending strings, as move_x,200
A sample control code written by python 3 is provived in here. An excutable file also provide for windows user for testing.

## Getting Started

### Dependencies

* arduino Uno
* stepper controller (CNC shield + A4988 or TB6600)
* 2 phase 4 wire stepper \
* limit switch
* (python)


### Installing

* burn the MotorSerialControl.ino into your Uno

### comment string
To string with terminator"," is used to send to Uno.
home_x,

To run a motor, number of steps should follow the ",". Only "move" comment need to add number.
Ex: move_x,200 

A string type message will send out when a function finish.


### Executing program

* pysimplegui is need for the gui display.


## Authors
Tina

