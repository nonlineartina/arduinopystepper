"""
This code is for controling two motors through arduino Uno.
The paired coed of arduino is MotorSerialControl_Tina.ino, please use the newest version.
Comment string will send through pyserial, than return a string when called function finished.

code by Tina @ ADV
"""

# 01 send a string

import PySimpleGUI as sg    
import serial
from time import sleep
import serial.tools.list_ports


sg.theme('DarkAmber')    # Keep things interesting for your users

layout = [[sg.Text('enter string,(position)')],      
          [sg.Input(key='-IN-')],
          [sg.Text('show message:'),sg.Text(text = "",size = (20,1),key='-out-')],
          [sg.Button('submit'), sg.Exit()]]      

window = sg.Window('window to submit serial comment', layout)    


def ser_bytearray_fun(func_comment_str):
    """
    string to ascii
    :param comment_str : comment string of tanlian control card
    :type img:string
    ========
    Returns: func_comment_str: ascii
    """
    func_comment_str = func_comment_str+'\r\n'
    return func_comment_str.encode('ascii', 'ignore')


ports = list(serial.tools.list_ports.comports())
port_list = []
for p in ports:
    if 'Uno' in p[1]:
        COM_PORT = p[0]
print(COM_PORT)

BAUD_RATES = 57600

sleep(0.2)

ser = serial.Serial(COM_PORT, BAUD_RATES)  

while True:                             # The Event Loop
    event, values = window.read(timeout=100)      
    if event == sg.WIN_CLOSED or event == 'Exit':
        window.close()
        ser.close()  
        break      
    
    if event == 'submit':
        comment_str = text_input = values['-IN-']  
        ser.write(ser_bytearray_fun(comment_str)) 
        sleep(0.5)
    
    while ser.in_waiting:
        mcu_feedback = ser.readline().decode()  # 接收回應訊息並解碼
        window['-out-'].update(mcu_feedback)
    

