"""
This code is for controling two motors through arduino Uno.
The paired coed of arduino is MotorSerialControl_Tina.ino, please use the newest version.
Comment string will send through pyserial, than return a string when called function finished.

code by Tina @ ADV
"""

# 01 send a string
# 02 use botton instead of typing

import PySimpleGUI as sg    
import serial
from time import sleep
import serial.tools.list_ports


sg.theme('DarkAmber')    # Keep things interesting for your users

layout = [[sg.Button('home_x', size=(10, 1), font='Helvetica 12'),
           sg.Button('home_y', size=(10, 1), font='Helvetica 12')],
          [sg.Button('move_x', size=(10, 1), font='Helvetica 12'),
           sg.Button('move_y', size=(10, 1), font='Helvetica 12'),
           sg.Button('move_u', size=(10, 1), font='Helvetica 12'),
           sg.Button('move_v', size=(10, 1), font='Helvetica 12')],
          [sg.Button('info_x', size=(10, 1), font='Helvetica 12'),
           sg.Button('info_y', size=(10, 1), font='Helvetica 12'),
           sg.Button('isRun_x', size=(10, 1), font='Helvetica 12'),
            sg.Button('isRun_y', size=(10, 1), font='Helvetica 12')],
          [sg.Button('stop_x', size=(10, 1), font='Helvetica 12'),
           sg.Button('stop_y', size=(10, 1), font='Helvetica 12'),
           sg.Button('dis_xy', size=(10, 1), font='Helvetica 12'),],
          [sg.Text('distance'), sg.Input(key='-distance-'), sg.Button('AddDist')],
          [sg.Text('string to send: '), sg.Text(text = "", key='-Send-')],
          [sg.Text('show message: '),sg.Text(text = "",size = (20,1),key='-Read-')],
          [sg.Button('submit'), sg.Exit()]]      

window = sg.Window('window to submit serial comment', layout)    


def ser_bytearray_fun(func_comment_str):
    """
    string to ascii
    :param comment_str : comment string of tanlian control card
    :type img:string
    ========
    Returns: func_comment_str: ascii
    """
    func_comment_str = func_comment_str+'\r\n'
    return func_comment_str.encode('ascii', 'ignore')


ports = list(serial.tools.list_ports.comports())
port_list = []
for p in ports:
    if 'Uno' in p[1]:
        COM_PORT = p[0]
print(COM_PORT)

BAUD_RATES = 57600

sleep(0.2)

ser = serial.Serial(COM_PORT, BAUD_RATES)  

comment = None

while True:                             # The Event Loop
    event, values = window.read(timeout=100)  
    if event == sg.WIN_CLOSED or event == 'Exit':
        
        ser.write(ser_bytearray_fun('dis_xy,')) 
        sleep(0.1)
        mcu_feedback = ser.readline().decode()  # 接收回應訊息並解碼
        window['-Read-'].update(mcu_feedback)
        sleep(0.1)
        window.close()
        ser.close()  
        
        break      
    
    if event == 'home_x':
        comment ='home_x,'
        window['-Send-'].update(comment)
        
    if event == 'home_y':
        comment ='home_y,'
        window['-Send-'].update(comment)
        
    if event == 'move_x':
        comment ='move_x,'
        window['-Send-'].update(comment)
        
    if event == 'move_y':
        comment ='move_y,'
        window['-Send-'].update(comment)
        
    if event == 'move_u':
        comment ='move_u,'
        window['-Send-'].update(comment)
        
    if event == 'move_v':
        comment ='move_v,'
        window['-Send-'].update(comment)
        
    if event == 'info_x':
        comment ='info_x,'
        window['-Send-'].update(comment)
        
    if event == 'info_y':
        comment ='info_y,'
        window['-Send-'].update(comment)

    if event == 'state_xy':
        comment ='state_xy,'
        window['-Send-'].update(comment)

    if event == 'isRun_x':
        comment ='isRun_x,'
        window['-Send-'].update(comment)
        
    if event == 'isRun_y':
        comment ='isRun_y,'
        window['-Send-'].update(comment)
        
    if event == 'stop_x':
        comment ='stop_x,'
        window['-Send-'].update(comment)
        
    if event == 'stop_y':
        comment ='stop_y,'
        window['-Send-'].update(comment)
    if event == 'dis_xy':
        comment ='dis_xy,'
        window['-Send-'].update(comment)
        
    if event == 'AddDist':
        comment = comment + values['-distance-']
        window['-Send-'].update(comment)
                

    if event == 'submit': 
        ser.write(ser_bytearray_fun(comment)) 
        sleep(0.5)
    
    while ser.in_waiting:
        mcu_feedback = ser.readline().decode()  # 接收回應訊息並解碼
        window['-Read-'].update(mcu_feedback)
    

