/* source code here
 * https://curiousscientist.tech/blog/accelstepper-tb6600-homing 
 */

#include <AccelStepper.h>

const int xdirPin = 5;     // 方向控制引脚
const int xstepPin = 2;    // 步进控制引脚
const int xenablePin = 8;  // 使能控制引脚
const int ydirPin = 6;     // 方向控制引脚
const int ystepPin = 3;    // 步进控制引脚 
const int x_limit = 9; //x 方向限位開關 normal close 串聯
const int y_limit = 10; //y 方向限位開關

long receivedMMdistance = 0; //distance in mm from the computer
String receivedCommand; //character for commands
char terminatorChar = ','; 
long CurrentPos_x;
long TargetPos_x;
long DistToGo_x;
long CurrentPos_y;
long TargetPos_y;
long DistToGo_y;
bool IsRunning_x;
bool IsRunning_y;
int state = 0;
 
bool newData = false; // booleans for new data from serial, and runallowed flag
const byte ledPin = 13; //led status pin, just to get a visual feedback from the button

AccelStepper stepper_x(1,xstepPin,xdirPin);
AccelStepper stepper_y(1,ystepPin,ydirPin);


void setup()
{
  //pin define
  pinMode(xstepPin,OUTPUT);     // Arduino控制A4988步进引脚为输出模式
  pinMode(xdirPin,OUTPUT);      // Arduino控制A4988方向引脚为输出模式
  pinMode(ystepPin,OUTPUT);     // Arduino控制A4988步进引脚为输出模式
  pinMode(ydirPin,OUTPUT);      // Arduino控制A4988方向引脚为输出模式
  pinMode(xenablePin,OUTPUT);   // Arduino控制A4988使能引脚为输出模式
  digitalWrite(xenablePin,HIGH); // 将使能控制引脚设置为低电平从而让电机驱动板进入工作状态
  pinMode(x_limit, INPUT_PULLUP); 
  pinMode(y_limit, INPUT_PULLUP);

  //LED pins, OFF by default
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);  
  
  // set board rate
  Serial.begin(57600); //define baud rate
//  Serial.println("arduino uno + A4988 + CNCshield"); //print a message

  //setting up some default values for maximum speed and maximum acceleration
  stepper_x.setMaxSpeed(2000); //SPEED = Steps / second
  stepper_x.setAcceleration(1000); //ACCELERATION = Steps /(second)^2
  stepper_y.setMaxSpeed(2000); //SPEED = Steps / second
  stepper_y.setAcceleration(1000); //ACCELERATION = Steps /(second)^2
}

void loop(){
  checkSerial(); //check serial port for new commands
}

// sub function


void runNoLimit_x(){
  while(stepper_x.distanceToGo() != 0){
    stepper_x.setSpeed(1000); 
    stepper_x.runSpeedToPosition();
  }
}

void runNoLimit_y(){
  while(stepper_y.distanceToGo() != 0){
    stepper_y.setSpeed(1000); 
    stepper_y.runSpeedToPosition();
  }
}

void runWithLimit_x(){
  while(stepper_x.distanceToGo() != 0){
    stepper_x.setSpeed(1000); 
    checkSerial();
    stepper_x.runSpeedToPosition();
    if (digitalRead(x_limit) == HIGH){
      break;
      }
  }
  delay(200);
  if (digitalRead(x_limit) == HIGH){
      if (stepper_x.currentPosition()< 2000){
        stepper_x.move(200);
        runNoLimit_x();
      }
      if (stepper_x.currentPosition()> 2000){
        stepper_x.move(-200);
        runNoLimit_x();
      }
      }
}

void runWithLimit_y(){
  while(stepper_y.distanceToGo() != 0){
    stepper_y.setSpeed(1000); 
    checkSerial();    
    stepper_y.runSpeedToPosition();

    if (digitalRead(y_limit) == HIGH){
      break;
      }
  }
  delay(200);
  if (digitalRead(y_limit) == HIGH){
      if (stepper_y.currentPosition()< 2000){
        stepper_y.move(200);
        runNoLimit_y();
      }
      if (stepper_y.currentPosition()> 2000){
        stepper_y.move(-200);
        runNoLimit_y();
      }
      }
}


void HomeX(){
  
  digitalWrite(xenablePin,LOW);
  stepper_x.setCurrentPosition(0);
  stepper_x.moveTo(-200000);
  while(digitalRead(x_limit) == LOW){
    stepper_x.setSpeed(1000); 
    stepper_x.runSpeedToPosition();
  }
  stepper_x.setCurrentPosition(0);
}


void HomeY(){
  digitalWrite(xenablePin,LOW);
  stepper_y.setCurrentPosition(0);
  stepper_y.moveTo(-1*200000);
  while(digitalRead(y_limit) == LOW){
    stepper_y.setSpeed(1000); 
    stepper_y.runSpeedToPosition();
  }
  stepper_y.setCurrentPosition(0);
}

void checkSerial() //method for receiving the commands
{  
  //switch-case would also work, and maybe more elegant
  
  if (Serial.available() > 0) //if something comes
  {
    receivedCommand = Serial.readStringUntil(terminatorChar);  // this will read the command character
    newData = true; //this creates a flag
  }

  if (newData == true) //if we received something (see above)
  {
    // test mode, run 5mm
    if (receivedCommand == String("move_test")) //test runnung
    {
      digitalWrite(xenablePin,LOW);
      CurrentPos_x = stepper_x.currentPosition(); //value for the steps
      stepper_x.moveTo(1000+CurrentPos_x);
      TargetPos_x = stepper_x.targetPosition () ; //value for the steps
      delay(100);
      runWithLimit_x();

      digitalWrite(xenablePin,LOW);
      CurrentPos_y = stepper_y.currentPosition(); //value for the steps
      stepper_y.moveTo(1000+CurrentPos_y);
      TargetPos_y = stepper_y.targetPosition () ; //value for the steps
      delay(100);
      runWithLimit_y();
      delay(100);
      digitalWrite(xenablePin,HIGH);
      Serial.println("finish test");
    }
    
    //x go absolute
    if (receivedCommand == String("move_x")) //this is the measure part
    {
      digitalWrite(xenablePin,LOW);
      receivedMMdistance = Serial.parseFloat(); //value for the steps
      CurrentPos_x = stepper_x.currentPosition(); //value for the steps
      stepper_x.moveTo(receivedMMdistance);
      delay(100);
      runWithLimit_x();
      delay(100);
      CurrentPos_x = stepper_x.currentPosition();
      digitalWrite(xenablePin,HIGH);
      Serial.print("x at ");
      Serial.println(CurrentPos_x);
    }

    //y go absolute
    if (receivedCommand == String("move_y")) //this is the measure part
    {
      digitalWrite(xenablePin,LOW);
      receivedMMdistance = Serial.parseFloat(); //value for the steps
      CurrentPos_y = stepper_y.currentPosition(); //value for the steps
      stepper_y.moveTo(receivedMMdistance);
      delay(100);
      runWithLimit_y();
      delay(100);
      CurrentPos_y = stepper_y.currentPosition();
      digitalWrite(xenablePin,HIGH);
      Serial.print("y at ");
      Serial.println(CurrentPos_y);
    }

    //x go relateve
    if (receivedCommand == String("move_u")) //this is the measure part
    {
      digitalWrite(xenablePin,LOW);
      receivedMMdistance = Serial.parseFloat(); //value for the steps
      CurrentPos_x= stepper_x.currentPosition(); //value for the steps
      stepper_x.moveTo(receivedMMdistance + CurrentPos_x);
      delay(100);
      runWithLimit_x();
      delay(100);
      CurrentPos_x = stepper_x.currentPosition();
      digitalWrite(xenablePin,HIGH);
      Serial.print("x at ");
      Serial.println(CurrentPos_x);
    }

    //y go relateve
    if (receivedCommand ==  String("move_v")) //this is the measure part
    {
      digitalWrite(xenablePin,LOW);
      receivedMMdistance = Serial.parseFloat(); //value for the steps
      CurrentPos_y = stepper_y.currentPosition(); //value for the steps
      stepper_y.moveTo(receivedMMdistance + CurrentPos_y);
      delay(100);
      runWithLimit_y();
      delay(100);
      CurrentPos_y = stepper_y.currentPosition();
      digitalWrite(xenablePin,HIGH);
      Serial.print("y at ");
      Serial.println(CurrentPos_y);
    }

    //HOMING x
    if (receivedCommand ==  String("home_x")) //homing X
    {
      digitalWrite(xenablePin,LOW);
      HomeX();
      delay(200);
      stepper_x.moveTo(200);
      runNoLimit_x();
      stepper_x.setCurrentPosition(0);
      digitalWrite(xenablePin,HIGH);
      Serial.println("Home x");
    }

    //HOMING y
    if (receivedCommand == String("home_y")) //homing X
    {
      digitalWrite(xenablePin,LOW);
      HomeY();
      delay(200);
      stepper_y.moveTo(200);
      runNoLimit_y();
      stepper_y.setCurrentPosition(0);
      digitalWrite(xenablePin,HIGH);
      Serial.println("Home y");
    }


    //write out x information
    if (receivedCommand == String("info_x")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      CurrentPos_x = stepper_x.currentPosition(); //value for the steps
      Serial.println(CurrentPos_x);  //print the values for checking
    }

    //write out y information
    if (receivedCommand == String("info_y")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      CurrentPos_y = stepper_y.currentPosition(); //value for the steps
      Serial.println(CurrentPos_y);  //print the values for checking
    }

  //write out x information
    if (receivedCommand == String("isRun_x")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      Serial.println(stepper_x.distanceToGo());
    }

    //write out y information
    if (receivedCommand == String("isRun_y")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      Serial.println(stepper_y.distanceToGo());
    }
    
  //stop x
    if (receivedCommand == String("stop_x")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
     stepper_x.stop();
     Serial.println("stop x");
    }

    //stop y
    if (receivedCommand == String("stop_y")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      stepper_y.stop(); //value for the steps
      Serial.println("stop y");
    }
    
    //disable motor
    if (receivedCommand == String("dis_xy")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      digitalWrite(xenablePin,HIGH);  //value for the steps
      Serial.println("disable motors");  //print the values for checking
    }

    // motor state
    if (receivedCommand == String("state_xy")) //CLOSING - Rotates the motor in the opposite direction as opening
    {
      state = digitalRead(xenablePin);  //value for the steps
      Serial.println(state);  //print the values for checking
    }

  }

  //after we went through the above tasks, newData becomes false again, so we are ready to receive new commands again.
  newData = false;
}
